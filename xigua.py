import requests
import re
from base64 import b64decode, b64encode

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36'
}

url = 'https://www.ixigua.com/6881172092222439950'

resp =requests.get(url=url, headers=headers)

ex = '''"main_url":"(.*?)"'''

def doCheck(source):
    if bytes(source, encoding='utf-8') == b64encode(b64decode(source)):
        return True
    return False

def getVideoUrl(source):
    return b64decode(source).decode('utf-8')

source =re.findall(ex, resp.text)[0]


if doCheck(source):
    resp = requests.get(url=getVideoUrl(source), headers=headers)
    with open('./Video.mp4', 'wb') as fp:
        fp.write(resp.content)
else:
    print('该Url不是Base64加密!!!')
